﻿
using First;

var publishings = new List<Publishing>();

for (int n = 0; n < 10; n++)
{
    publishings.Add(new Book("Book " + n));
    publishings.Add(new Article("Article " + n));
    publishings.Add(new Article("Digital Resource " + n));
}

foreach (var item in publishings)
{
    Console.WriteLine(item.GetDetails());
}
while (true)
{
    Console.WriteLine("Enter start date (DD/MM/YYYY)");
    var startDate = DateTime.Parse(Console.ReadLine());
    Console.WriteLine("Enter end date (DD/MM/YYYY)");
    var endDate = DateTime.Parse(Console.ReadLine());

    var result = publishings.Where(x => x.PublishDate >= startDate && x.PublishDate < endDate);
    if (result.Any())
    {
        foreach (var res in result)
        {
            Console.WriteLine(res.GetDetails());
        }
    }
    else
    {
        Console.WriteLine("Record not found!");
    }
}
   
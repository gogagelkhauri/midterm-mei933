﻿namespace First;

public class DigitalResource : Publishing
{
    public string Name { get; set; }
    public List<string> Authors { get; set; }
    public string Link { get; set; }
    public string Anotation { get; set; }

    public DigitalResource(string details) : base(details)
    {
    }
}
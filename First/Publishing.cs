﻿namespace First;

public abstract class Publishing
{
    public string Name { get; set; }
    public DateTime PublishDate { get; set; }
    public string Details { get; set; }

    public Publishing(string details)
    {
        Details = details;
        PublishDate = DateTime.Now;
    }

    public string GetDetails()
    {
        return Details + "-" + PublishDate;
    }
}
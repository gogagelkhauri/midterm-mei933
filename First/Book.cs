﻿namespace First;

public class Book : Publishing
{
    public string Name { get; set; }
    public List<string> Authors { get; set; }
    public int Numeration { get; set; }
    public int PublishYear { get; set; }
    public int Pages { get; set; }

    public Book(string details) : base(details)
    {
    }
}
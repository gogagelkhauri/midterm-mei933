﻿using System.Security.AccessControl;

namespace First;

public class Article : Publishing
{
    public string Name { get; set; }
    public List<string> Authors { get; set; }
    public string JournalName { get; set; }
    public int Numeration { get; set; }
    public int PublishYear { get; set; }
    public int Pages { get; set; }

    public Article(string details) : base(details)
    {
    }
}
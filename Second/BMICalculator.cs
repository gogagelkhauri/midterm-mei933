﻿using Second.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Second
{
    public class BMICalculator : IBMICalculator
    {
        public double CalculateBMI(double weight, double height)
        {
            double bmi = weight / (height * height);
            return bmi;
        }
    }
}

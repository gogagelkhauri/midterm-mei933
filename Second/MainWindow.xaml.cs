﻿using Second.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Second
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IBMICalculator _bmiCalculator;
        public MainWindow()
        {
            InitializeComponent();
            _bmiCalculator = new BMICalculator();
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            var weight = Double.Parse(textBox.Text);
            var height = Double.Parse(textBox1.Text);

            var result = _bmiCalculator.CalculateBMI(weight, height);
            label.Content = result.ToString();
            if(result < 16)
            {
                label2.Content = "წონის მწვავე დეფიციტი";
            }
            else if (result > 16 && result < 18.5)
            {
                label2.Content = "წონის დეფიციტი";
            }
            else if (result > 18.5 && result < 25)
            {
                label2.Content = "ნორმალური წონა";
            }
            else if (result > 25 && result < 30)
            {
                label2.Content = "ზედმეტი წონა";
            }
            else if(result > 30)
            {
                label2.Content = "სიმსუქნე";
            }
        }

    }
}